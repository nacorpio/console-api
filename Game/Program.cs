﻿using System;
using ConsoleAPI;
using ConsoleAPI.Elements;

namespace Game {

    public class Program {

        public static readonly MenuHandler MenuHandler = new MenuHandler();
        public static readonly Menu MainMenu = new Menu("MainMenu") {
            Caption = "Main Menu"
        };

        static void Main(string[] args) {

            var button1 = new ElementButton("Start new game");
            var button2 = new ElementButton("Load game");

            var sep1 = new ElementSeparator();

            var button3 = new ElementButton("Settings");

            var sep2 = new ElementSeparator();

            var button4 = new ElementButton("Quit");
            button4.Activated += (sender, eventArgs) => {Environment.Exit(0);};

            MainMenu.AddElements(button1, button2, sep1, button3, sep2, button4);

            MenuHandler.AddMenu(MainMenu);
            MenuHandler.ShowMenu("MainMenu");

            while (true) {
                var key = Console.ReadKey().Key;
                MenuHandler.ProcessKeys(key);
            }

        }

    }

}
