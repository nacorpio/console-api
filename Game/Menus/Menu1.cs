﻿using System;
using ConsoleAPI;
using ConsoleAPI.Elements;

namespace Game.Menus {

    public class Menu1 : Menu {

        public Menu1() : base("MainMenu") {

            Caption = "Main Menu";

            var button1 = new ElementButton("Start New Game");
            button1.Activated += ButtonStartNewGameOnActivated;

            var button2 = new ElementButton("Load Game");
            button2.Activated += ButtonLoadGameOnActivated;

            var separator1 = new ElementSeparator();

            var button3 = new ElementButton("Settings");
            button3.Activated += ButtonSettingsOnActivated;

            var separator2 = new ElementSeparator();

            var button4 = new ElementButton("Exit");
            button4.Activated += ButtonExitOnActivated;

            AddElements(button1, button2, separator1, button3, separator2, button4);

        }

        private void ButtonExitOnActivated(object sender, EventArgs eventArgs) {
            
        }

        private void ButtonSettingsOnActivated(object sender, EventArgs eventArgs) {
            throw new NotImplementedException();
        }

        private void ButtonLoadGameOnActivated(object sender, EventArgs eventArgs) {
            throw new NotImplementedException();
        }

        private void ButtonStartNewGameOnActivated(object sender, EventArgs eventArgs) {
            throw new NotImplementedException();
        }
    }

}
