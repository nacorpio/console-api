﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ConsoleAPI {

    public sealed class ConsoleUtilities {

        public class ColorElement {

            private ConsoleColor _color = ConsoleColor.Gray;

            public ColorElement(string content, string color) {
                Content = content;
                StringColor = color;
            }

            public string Content { get; set; }

            public string StringColor { get; set; }

            public ConsoleColor Color {
                get { return StringToColor(StringColor); }
                set { _color = value; }
            }

        }

        public static void WriteFormattedLine(string text) {
            
            // {@:yellow}
            // {@:green}

            for (var i = 0; i < GetColorCount(text); i++){

                var element = GetColorElement(text, i);

                Console.ForegroundColor = element.Color;
                Console.Write(element.Content);
                Console.ForegroundColor = ConsoleColor.Gray;

            }

        }

        public static ConsoleColor StringToColor(string color) {
            switch (color.ToLower()){
                case "yellow":
                    return ConsoleColor.Yellow;
                case "green":
                    return ConsoleColor.Green;
                case "gray":
                    return ConsoleColor.Gray;
                case "white":
                    return ConsoleColor.White;
            }
            return ConsoleColor.Gray;
        }

        public static int GetColorCount(string data) {
            return new Regex("{@color:(.*?)}").Matches(data).Count;
        }

        public static string GetContentOfTag(string data, int index) {

            var colorRegex = new Regex("{@color:(.*?)}");
            var endTagRegex = new Regex("{@}");

            var colorMatch = colorRegex.Matches(data)[index];
            var endTagMatch = endTagRegex.Matches(data)[index];

            var colorIndex = colorMatch.Index;
            var endTagIndex = endTagMatch.Index;

            data = data.Remove(colorIndex, colorMatch.Length).Remove(endTagIndex - colorMatch.Length, endTagMatch.Length);
            return data;

        }

        public static ColorElement GetColorElement(string data, int index) {
            return ParseColorElement(new Regex("{@color:(.*?)}(.*?){@}").Matches(data)[index].Value);
        }

        public static ColorElement ParseColorElement(string data) {
            
            var regex = new Regex("{@color:(.*?)}(.*?){@}");

            var color = regex.Matches(data)[0].Groups[1].Value;
            var content = regex.Matches(data)[0].Groups[2].Value;

            return new ColorElement(content, color);


        }

        public static int IndexOfColor(string data, string color, int startIndex) {
            return data.IndexOf("{@color:" + color + "}", startIndex, StringComparison.CurrentCulture);
        }

        public static int IndexOfEndTag(string data, int startIndex) {
            return data.IndexOf("{@}", startIndex, StringComparison.CurrentCulture);
        }

        public static string ParseFormattedColor(string data) {
            return Regex.Match(data, "{@:(.*?)}").Groups[1].Value;
        }

        public static bool IsFormattedColor(string data) {
            return Regex.IsMatch(data, "{@(.*?)}");
        }

        public static void WriteLine(string text, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

    }

}
