﻿using System;
using EzF;

namespace ConsoleAPI {

    class Program {

        public static readonly MenuHandler MenuHandler = new MenuHandler();
        public static readonly Menu MainMenu = new Menu("MainMenu");

        static void Main(string[] args) {

            var button1 = new Elements.ElementButton("A button, obviously");
            MainMenu.AddElement(button1);

            MenuHandler.AddMenu(MainMenu);
            MenuHandler.ShowMenu("MainMenu");

            while (true){
                var key = Console.ReadKey().Key;
                MenuHandler.ProcessKeys(key);
            }

        }

    }

}
