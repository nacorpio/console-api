﻿using System;

namespace ConsoleAPI.Elements {

    public delegate void ClickedEventHandler(object sender, EventArgs e);

    public class ElementButton : Element {

        private string _caption;

        public ElementButton(string caption) : base("Button") {
            _caption = caption;
        }

        public override bool IsIgnored() {
            return false;
        }

        public string Caption {
            get { return _caption; }
            set {
                _caption = value;
                if (ParentMenu != null)
                    ParentMenu.Draw();
            }
        }

        public override void Draw() {
            
            if (IsSelected){
                Console.ForegroundColor = SelectedColor;
                Console.WriteLine("  > " + Caption);
                Console.ForegroundColor = ConsoleColor.Gray;
                return;
            }

            Console.WriteLine("   " + Caption);

        }

    }

}
