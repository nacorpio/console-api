﻿using System;

namespace ConsoleAPI.Elements {

    public class ElementSeparator : Element {

        private string _caption;

        public ElementSeparator() : base("Separator") {}

        public ElementSeparator(string caption) : base("Separator") {
            _caption = caption;
        }

        public string Caption {
            get {
                return _caption;
            }
            set {
                _caption = value;
                if (ParentMenu != null)
                    ParentMenu.Draw();
            }
        }

        public override bool IsIgnored() {
            return true;
        }

        public override void Draw() {

            if (string.IsNullOrWhiteSpace(_caption)){
                Console.WriteLine("-------------------------");
                return;
            }

            Console.WriteLine(" " + _caption);

        }
    }

}
