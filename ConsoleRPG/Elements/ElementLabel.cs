﻿using System;

namespace ConsoleAPI.Elements {

    public class ElementLabel : Element {

        private string _caption;

        public ElementLabel(string caption) : base("Label") {
            _caption = caption;
        }

        public string Caption {
            get { return _caption; }
            set {
                _caption = value;
                if (ParentMenu != null)
                    ParentMenu.Draw();
            }
        }

        public override void Draw() {
            Console.WriteLine("   " + _caption);
        }

        public override bool IsIgnored() {
            return true;
        }

    }

}
