﻿using System;

namespace ConsoleAPI.Elements {

    public class ElementSwitch : Element {

        private string _caption;
        private string[] _items;

        private int _selectedIndex;

        public ElementSwitch(string caption, params string[] items) : base("Switch") {
            _caption = caption;
            _items = items;
            _selectedIndex = 0;
        }

        public override bool IsIgnored() {
            return false;
        }

        public override void Draw() {

            if (IsSelected) {
                Console.ForegroundColor = SelectedColor;
                Console.WriteLine("  > " + Caption + " <" + _items[_selectedIndex] + ">");
                Console.ForegroundColor = ConsoleColor.Gray;
                return;
            }

            Console.WriteLine("   " + Caption + " <" + _items[_selectedIndex] + ">");

        }

        public void Next() {

            if (_selectedIndex + 1 <= _items.Length - 1){
                _selectedIndex++;
            } else{
                _selectedIndex = 0;
            }

            Draw();

        }

        public void Previous() {

            if (_selectedIndex - 1 >= 0){
                _selectedIndex--;
            } else{
                _selectedIndex = _items.Length - 1;
            }

            Draw();

        }

        public int SelectedIndex {
            get {
                return _selectedIndex;
            }
            set {
                _selectedIndex = value;
                if (ParentMenu != null)
                    ParentMenu.Draw();
            }
        }

        public string[] Items {
            get { return _items; }
            set {
                _items = value;
                if (ParentMenu != null)
                    ParentMenu.Draw();
            }
        }

        public string Caption {
            get {
                return _caption;
            }
            set {
                _caption = value;
                if (ParentMenu != null)
                    ParentMenu.Draw();
            }
        }

    }

}
