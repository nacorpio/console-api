﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleAPI.Elements;

namespace ConsoleAPI {

    public delegate void MenuEventHandler(object sender, EventArgs e);
    public delegate void SelectionEventHandler(int newIndex, object sender, EventArgs e);

    public class Menu {

        public event SelectionEventHandler SelectionChanged;
        public event SelectionEventHandler Next, Previous;

        private Dictionary<string, ConsoleKey> _keys = new Dictionary<string, ConsoleKey>();
        private int _selectedIndex;

        public Menu(string id) {
            Id = id;
        }

        public string Id {
            get; set;
        }

        public string Caption {
            get; set;
        }

        public ConsoleColor CaptionColor {
            get; set;
        }
        = ConsoleColor.White;

        public ConsoleColor KeyColor {
            get; set;
        }
        = ConsoleColor.Green;

        public List<Element> Elements {
            get;
        }
        = new List<Element>();

        public Menu PreviousMenu {
            get; set;
        }

        public int SelectedIndex {
            get {
                return _selectedIndex;
            }
            set {

                var increment = value > _selectedIndex;
                var decrement = value < _selectedIndex;

                _selectedIndex = value;

                var currentElement = Elements[value];

                if (currentElement.IsIgnored() && increment){

                    var newValue = value + 1;

                    if (newValue > Elements.Count - 1){
                        newValue = 0;
                    }

                    currentElement = Elements[newValue];
                    _selectedIndex = newValue;

                }

                if (currentElement.IsIgnored() && decrement){

                    var newValue = value - 1;

                    if (newValue < 0){
                        newValue = Elements.Count - 1;
                    }

                    currentElement = Elements[newValue];
                    _selectedIndex = newValue;

                }

                Elements.ForEach(e => {
                    if (e.IsSelected)
                        e.Deselect();
                });

                currentElement.Select();

                if (SelectionChanged != null) {
                    SelectionChanged(value, this, EventArgs.Empty);
                }

            }
        }

        private bool IsIgnoredElement(Element element) {
            return IsIgnoredElement(element.Id);
        }

        private bool IsIgnoredElement(string id) {
            return Elements.Any(e => e.Id == id && e.IsIgnored());
        }

        private bool IsIgnoredElement(int index) {
            return IsIgnoredElement(Elements[index]);
        }

        public bool ContainsElement(string id) {
            return Elements.Any(e => e.Id == id);
        }

        public void AddKeys(Dictionary<string, ConsoleKey> keys) {
            for (var i = 0; i < keys.Count; i++){

                var key = keys.Keys.ToList()[i];
                var value = keys.Values.ToList()[i];

                AddKey(key, value);

            }
        }

        public void AddKey(string action, ConsoleKey key) {

            if (string.IsNullOrWhiteSpace(action)) {
                throw new Exception("The action string can not be empty.");
            }

            if (_keys.ContainsValue(key)) {
                throw new Exception("The specified key has already been mapped.");
            }

            _keys.Add(action, key);

        }

        public void AddElements(params Element[] elements) {
            elements.ToList().ForEach(e => AddElement(e));
        }

        public void AddElement(Element element) {
            element.ParentMenu = this;
            Elements.Add(element);
            SelectedIndex = 0;
        }

        public void NextElement() {

            var maxIndex = Elements.Count - 1;

            if (SelectedIndex + 1 <= maxIndex) {
                SelectedIndex++;
            } else {
                SelectedIndex = 0;
            }

            if (Next != null) {
                Next(SelectedIndex, this, EventArgs.Empty);
            }

            Draw();

        }

        public void PreviousElement() {

            if (SelectedIndex - 1 >= 0) {
                SelectedIndex--;
            } else {
                SelectedIndex = Elements.Count - 1;
            }

            if (Previous != null) {
                Previous(SelectedIndex, this, EventArgs.Empty);
            }

            Draw();

        }

        public void ProcessKey(ConsoleKey key) {

            switch (key) {
                case ConsoleKey.DownArrow:
                    NextElement();
                    break;
                case ConsoleKey.UpArrow:
                    PreviousElement();
                    break;
                case ConsoleKey.Enter:
                    Elements[SelectedIndex].Activate();
                    break;
            }

            if (Elements[SelectedIndex] is ElementSwitch) {
                var elementSwitch = (ElementSwitch)Elements[SelectedIndex];
                switch (key) {
                    case ConsoleKey.RightArrow:
                        elementSwitch.Next();
                        break;
                    case ConsoleKey.LeftArrow:
                        elementSwitch.Previous();
                        break;
                }
            }

            Draw();

        }

        public void Draw() {

            Console.Clear();

            Console.WriteLine();
            ConsoleUtilities.WriteLine(" " + Caption, CaptionColor);
            Console.WriteLine();

            Elements.ForEach(e => {

                if (e.NewLineAbove)
                    Console.WriteLine();

                e.Draw();

                if (e.NewLineBelow)
                    Console.WriteLine();

            });

            Console.WriteLine();

            if (_keys != null && _keys.Count > 0) {


                for (var i = 0; i < _keys.Values.Count; i++) {

                    var value = _keys.Values.ToList()[i];
                    var key = _keys.Keys.ToList()[i];

                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(" " + key + " ");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write("[");
                    Console.ForegroundColor = KeyColor;
                    Console.Write(value);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write("]  ");

                }

            }

        }

    }

}
