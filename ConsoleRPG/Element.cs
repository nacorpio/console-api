﻿using System;

namespace ConsoleAPI {

    public delegate void ElementEventHandler(object sender, EventArgs e);

    public abstract class Element {

        public event ElementEventHandler Activated;
        public event ElementEventHandler Selected;
        public event ElementEventHandler Deselected;

        protected Element(string id) {
            Id = id;
        }

        /// <summary>
        /// Returns the unique ID of this element.
        /// </summary>
        public string Id { get; private set; }

        public Menu ParentMenu { get; set; }

        /// <summary>
        /// Returns whether this element will create a new line below it.
        /// </summary>
        public bool NewLineBelow { get; set; }

        /// <summary>
        /// Returns whether this element will create a new line above it.
        /// </summary>
        public bool NewLineAbove { get; set; }

        /// <summary>
        /// Returns whether this element is selected in its menu.
        /// </summary>
        public bool IsSelected { get; private set; }

        /// <summary>
        /// Returns the color of this element when its selection state is set to true.
        /// </summary>
        public ConsoleColor SelectedColor { get; set; } = ConsoleColor.White;

        /// <summary>
        /// Draws this element in the console application.
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Returns whether this element shouldn't be selected within the menu.
        /// </summary>
        /// <returns></returns>
        public abstract bool IsIgnored();

        /// <summary>
        /// Activates this element in the console application.
        /// </summary>
        public void Activate() {
            if (Activated != null){
                Activated(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Selects this element within its menu.
        /// </summary>
        public void Select() {
            IsSelected = true;
            if (Selected != null){
                Selected(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Deselects this element within its menu.
        /// </summary>
        public void Deselect() {
            IsSelected = false;
            if (Deselected != null){
                Deselected(this, EventArgs.Empty);
            }
        }

    }

}
