﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleAPI {

    public sealed class MenuHandler {

        private Menu _currentMenu;
        private List<Menu> _menus = new List<Menu>();

        public Menu CurrentMenu {
            get {
                return _currentMenu;
            }
        }

        public int MenuCount {
            get { return _menus.Count; }
        }

        public void ProcessKeys(ConsoleKey key) {

            if (_currentMenu == null){
                throw new Exception("There is no menu currently being shown.");
            }

            _currentMenu.ProcessKey(key);

        }

        public void ShowMenu(Menu menu) {
            ShowMenu(menu.Id);
        }

        public void ShowMenu(string id) {

            if (_currentMenu != null && _currentMenu.Id == id){
                throw new Exception("A menu with that id is already being shown.");
            }

            var oldMenu = _currentMenu;

            _currentMenu = GetMenu(id);
            _currentMenu.PreviousMenu = oldMenu;
            _currentMenu.Draw();

        }

        public void ShowPreviousMenu() {

            if (_currentMenu == null || _currentMenu.PreviousMenu == null){
                throw new Exception("There is no menu or previous menu to show.");
            }

            ShowMenu(_currentMenu.PreviousMenu.Id);

        }

        public void AddMenus(params Menu[] menu) {
            menu.ToList().ForEach(m => {
                AddMenu(m);
            });
        }

        public void AddMenu(Menu menu) {

            if (ContainsMenu(menu.Id)){
                throw new Exception("A menu with that name already exists.");
            }

            _menus.Add(menu);

        }

        public Menu GetMenu(int index) {
            return _menus[index];
        }

        public Menu GetMenu(string id) {
            return _menus.Where(m => m.Id == id).ToList()[0];
        }

        public void RemoveMenu(string id) {

            if (!ContainsMenu(id)){
                throw new Exception("A menu with that name doesn't exist.");
            }

            _menus.Remove(GetMenu(id));

        }

        private bool ContainsMenu(string id) {
            return _menus.Any(m => m.Id == id);
        }

    }

}
