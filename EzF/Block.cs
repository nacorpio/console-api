﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EzF
{

    public class Block : Component {

        private readonly List<Section> _sections = new List<Section>(); 
        private readonly List<GlobalVariable> _globalVariables = new List<GlobalVariable>();

        private readonly Scope _scope;

        public Block(string name, Scope.ScopeVisibility visibility) : base(name) {
            _scope = new Scope(this, visibility);
        }

        public void AddSection(Section section) {

            if (section == null){
                throw new Exception("The section can not be null.");
            }

            if (ContainsSection(section.Name)){
                throw new Exception("A section with that name already exists.");
            }

            _sections.Add(section);
            _scope.AddName(section.Name, section);

        }

        public void AddGlobalVariable(GlobalVariable variable) {

            if (variable == null){
                throw new Exception("The global variable can not be null.");
            }

            if (_scope.NameExists(variable.Name)){
                throw new Exception("A component with the specified name already exists in this scope.");
            }

            if (ContainsGlobalVariable(variable.Name)){
                throw new Exception("A global variable with that name already exists.");
            }

            _globalVariables.Add(variable);
            _scope.AddName(variable.Name, variable);

        }

        public Section GetSection(Component component, string name) {

            if (string.IsNullOrWhiteSpace(name)){
                throw new Exception("The section name can not be empty or null.");
            }

            if (component.GetScope().Visibility == Scope.ScopeVisibility.Private){
                throw new Exception("The scope is private, and can not be accessed.");
            }

            return GetSection(name);
            
        }

        public Section GetSection(string name) {

            if (string.IsNullOrWhiteSpace(name)){
                throw new Exception("The section name can not be empty or null.");
            }

            if (!ContainsSection(name)){
                throw new Exception("A section with that name doesn't exist.");
            }

            return _sections.Where(s => s.Name == name).ToArray()[0];

        }

        public bool ContainsGlobalVariable(string name) {
            return _globalVariables.Any(v => v.Name == name);
        }

        public bool ContainsSection(string name) {
            return _sections.Any(s => s.Name == name);
        } 

        public IReadOnlyCollection<Section> GetSections() {
            return _sections.AsReadOnly();
        }

        public IReadOnlyCollection<GlobalVariable> GetGlobalVariables() {
            return _globalVariables.AsReadOnly();
        }

        public override string ToEzfString() {
            throw new NotImplementedException();
        }

        public override Scope GetScope() {
            return _scope;
        }
    }

}
