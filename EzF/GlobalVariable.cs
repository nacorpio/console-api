﻿
namespace EzF {

    public class GlobalVariable : Variable {

        public GlobalVariable(string name, object value, Scope.ScopeVisibility visibility) : base(name, value, visibility) {}

    }

}
