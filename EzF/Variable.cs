﻿namespace EzF {

    public class Variable : Component {

        private readonly Scope _scope;

        public Variable(string name, object value, Scope.ScopeVisibility visibility) : base(name) {
            Name = name;
            Value = value;
            _scope = new Scope(this, visibility);
        }

        public object Value { get; set; }

        public override Scope GetScope() {
            return _scope;
        }

        public override string ToEzfString() {
            return GetScope() + " def<" + Name + ">" + (Value != null ? " = " + Value : "") + ";";
        }
    }

}
