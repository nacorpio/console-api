﻿using System;
using System.Collections.Generic;

namespace EzF {

    public class Scope {

        public enum ScopeVisibility {
            Public, // The names are accessible from other scopes.
            Private // The names ar inaccessible from other scopes.
        }

        private Component _associatedComponent;

        private Dictionary<string, object> _names = new Dictionary<string, object>(); 
        private ScopeVisibility _visibility;

        public Scope(Component component, ScopeVisibility visibility) {
            _associatedComponent = component;
            _visibility = visibility;
        }

        public Component Component {
            get { return _associatedComponent; }
            set { _associatedComponent = value; }
        }

        public ScopeVisibility Visibility {
            get { return _visibility; }
            set { _visibility = value; }
        }

        public void AddName(string name, object value) {

            if (string.IsNullOrWhiteSpace(name)){
                throw new Exception("The name can not be empty or null.");
            }

            if (NameExists(name)){
                throw new Exception("A component with that name already exists.");
            }

            _names.Add(name, value);

        }

        public void RemoveName(string name) {

            if (string.IsNullOrWhiteSpace(name)){
                throw new Exception("The name of the component can not be empty.");
            }

            if (!NameExists(name)){
                throw new Exception("A component with that name doesn't exist.");
            }

            _names.Remove(name);

        }

        public bool CanAccess(Scope scope) {
            return scope.Visibility == ScopeVisibility.Public;
        }

        public bool NameExists(string name) {
            return _names.ContainsKey(name);
        }

        public override string ToString() {
            return _visibility.ToString().ToLower();
        }
    }

}
