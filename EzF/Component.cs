﻿
namespace EzF {

    public abstract class Component {

        private string _name;

        public Component(string name) {
            _name = name;
        }

        /// <summary>
        /// Returns the name of this component.
        /// </summary>
        public string Name {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Returns the scope of this component.
        /// </summary>
        /// <returns></returns>
        public abstract Scope GetScope();

        /// <summary>
        /// Returns the Ezf representation of this component.
        /// </summary>
        /// <returns></returns>
        public abstract string ToEzfString();

    }

}
