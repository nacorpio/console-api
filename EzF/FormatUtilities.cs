﻿using System.CodeDom;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;

namespace EzF {

    public sealed class FormatUtilities {

        /*

            block Block1() {

                section Section1() {

                    def Setting1;
                    def Setting2

                {/section}

            {/block}

            block Block2() {

                section Section1() {
                    
                    def<Setting1>;
                    def<Setting2> = Block1::Section1::Setting1;

                {/section}

            {/block}

        */

        public const string VariableRegex = "(public|private)?\\s*def<(.*?)>\\s*=\\s*(.*)?;";
        public const string SectionHeadRegex = "section(?<name>.*?)\\((?<pars>.*?)\\){(?<content>.*?){/section}";
        public const string BlockHeadRegex = "block(?<name>(.*?))\\(\\){(?<content>.*?){/block}";

        private static RegexOptions _regexOptions = RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline;

        public static string GetSectionName(string body) {
            return new Regex(SectionHeadRegex, _regexOptions).Matches(body)[0].Groups["name"].Value;
        }

        public static string GetSectionContent(string body) {
            return new Regex(SectionHeadRegex, _regexOptions).Matches(body)[0].Groups["content"].Value.Trim();
        }

        public static int SectionCount(string body) {
            return new Regex(SectionHeadRegex, _regexOptions).Matches(body).Count;
        }

        public static int VariableCount(string body) {
            return new Regex(VariableRegex, _regexOptions).Matches(body).Count;
        }

        public static List<Variable> ToVariables(string body) {
            
            var result = new List<Variable>();
            var regex = new Regex(VariableRegex, RegexOptions.IgnorePatternWhitespace);

            foreach (Match m in regex.Matches(body)){
                result.Add(ToVariable(m.Value));
            }

            return result;

        } 

        public static Variable ToVariable(string body) {

            // def Setting1 = "";
            // (1) def<(2)> = (3);

            var regex = new Regex(VariableRegex, RegexOptions.IgnorePatternWhitespace);
            var match = regex.Matches(body)[0];

            var name = match.Groups[2].Value;
            var value = match.Groups[3].Value;
            var scope = match.Groups[1].Value;

            object varValue = null;

            // Check if it's a float.
            if (value.ToCharArray()[value.Length - 1] == 'f'){
                varValue = float.Parse(value.Substring(0, value.Length - 1));
            }

            // Check if it's a double.
            if (value.ToCharArray()[value.Length - 1] == 'd'){
                varValue = double.Parse(value.Substring(0, value.Length - 1));
            }

            // Check if it's a string.
            if (value.ToCharArray()[0] == '"' && value.ToCharArray()[value.Length - 1] == '"'){
                varValue = value.Substring(1, value.Length - 2);
            }

            // Check if it's an array.
            if (value.ToCharArray()[0] == '{' && value.ToCharArray()[value.Length - 1] == '}'){
                var elements = Regex.Split(value.Substring(1, value.Length - 1), "\\s*,\\s*");
                varValue = elements;
            }

            return new Variable(name, varValue, (scope == "public" ? Scope.ScopeVisibility.Public : Scope.ScopeVisibility.Private));

        }

        public static int BlockCount(string body) {
            return new Regex("block(?<name>(.*?))\\(\\) {", RegexOptions.IgnorePatternWhitespace).Matches(body).Count;
        }

        public static string SectionToString(Section section) {
            return "";
        }

        public static string VariableToString(Variable variable) {
            return "";
        }

        public static string BlockToString(Block block) {
            return "";
        }

    }

}
