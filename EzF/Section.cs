﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EzF {

    public class Section : Component {

        private readonly List<Variable> _variables = new List<Variable>(); 
        private readonly Scope _scope;

        public Section(string name, Scope.ScopeVisibility visibility) : base(name) {
            _scope = new Scope(this, visibility);
        }

        public void AddVariable(Variable variable) {

            if (variable == null){
                throw new Exception("The variable can't be null.");
            }

            if (_scope.NameExists(variable.Name)){
                throw new Exception("A component with the specified name already exists in this scope.");
            }

            if (ContainsVariable(variable.Name)){
                throw new Exception("A variable with that name already exists.");
            }

            _variables.Add(variable);
            _scope.AddName(variable.Name, variable);

        }

        public Variable GetVariable(string name) {
            return _variables.Where(v => v.Name == name).ToArray()[0];
        }

        public bool ContainsVariable(string name) {
            return _variables.Any(v => v.Name == name);
        }

        public override Scope GetScope() {
            return _scope;
        }

        public override string ToEzfString() {
            throw new NotImplementedException();
        }
    }

}
